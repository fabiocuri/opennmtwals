# Train 
# En-Fr, bpe_endefr
# Attention, tgt
python train.py -data data/bpe_endefr -save_model data/bpe_endefr_att1 -wals_src eng -wals_tgt fre -train_steps 50000 -wals_model AttentionTarget -input_feed 0 -save_checkpoint_steps 50000

python train.py -data data/bpe_endefr -save_model data/bpe_endefr_att2 -wals_src eng -wals_tgt fre -train_steps 50000 -wals_model AttentionTarget -input_feed 0 -save_checkpoint_steps 50000

python train.py -data data/bpe_endefr -save_model data/bpe_endefr_att3 -wals_src eng -wals_tgt fre -train_steps 50000 -wals_model AttentionTarget -input_feed 0 -save_checkpoint_steps 50000

python train.py -data data/bpe_endefr -save_model data/bpe_endefr_att4 -wals_src eng -wals_tgt fre -train_steps 50000 -wals_model AttentionTarget -input_feed 0 -save_checkpoint_steps 50000
