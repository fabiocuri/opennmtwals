# Import libraries and set settings

import pandas as pd
from collections import defaultdict
import seaborn as sns
import matplotlib.colors as colors
import numpy as np
from collections import Counter
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from more_itertools import unique_everseen
import plotly.plotly as py
import plotly.graph_objs as go
import plotly.tools as tls
from plotly.graph_objs import *
from wals import w, langs, families, types, feature_types, elements
import random

plt.rcdefaults()
plt.style.use('ggplot')
sns.set_style("ticks")
sns.set_context("paper", rc={"axes.labelsize":36})
sns.set_style("whitegrid")

tls.set_credentials_file('fabiocuri', 'FxzMvcNB6ZRpjeWIsVQr')
py.sign_in('fabiocuri', 'FxzMvcNB6ZRpjeWIsVQr')
color_list = list(colors._colors_full_map.values())

# Get counts of discrete variables.

def language_plot(index, n):
    
    k, k_filtered, k_plot = defaultdict(lambda:0), defaultdict(lambda:0), defaultdict(lambda:0)
    
    for el in langs.keys():
        k[langs[el][index]]+=1
        k_filtered[langs[el][index]]+=1
        
    if k_filtered['other']:
        del k_filtered['other']
    
    top_elements = sorted(k_filtered.items(), key=lambda x: x[1], reverse=True)[:n]
    total=sum(k.values())
    count=0
    
    for key, val in top_elements:
        count+=val
        k_plot[key]=val
    k_plot['Others']=total-count

    return k_plot

# Get a random color list with n different elements.

def list_colors(n, color_list):
    
    random.shuffle(color_list)
    color_list = color_list[0:n]
    
    return color_list

# Get list of features that are at least perc % present among a dataframe of languages df.

def simultaneous_features(df, perc):
    
    simultaneous = defaultdict(lambda: 0)
    num_languages=len(df)
    
    for l in list(df['language']):
        for k in langs[l].keys():
                simultaneous[k]+=1    
        
    s = []

    for l in simultaneous.keys():
        if simultaneous[l]>=num_languages*perc:
            s.append(l)

    elements.append('%')
    for l in elements:
        if l in s: s.remove(l)
            
    return s

# Build plots for language family, GENUS and macroarea.

plots = ['family', 'genus', 'macroarea']
num = [15,5,5]

for index, n in zip(plots, num):
    vals_filtered = language_plot(index, n)
    label, label_value = [], []
    
    for key, val in zip(vals_filtered.keys(), vals_filtered.values()):
        label.append(key)
        label_value.append(val)
        
    f, ax1 = plt.subplots(1, 1, figsize=(12, 8), sharex=True)
    series = pd.Series(label_value, index=label, name='')
    series.plot.pie(figsize=(10, 10), colors=list_colors(n+1, color_list), autopct='%1.1f%%', fontsize=12)
    f.suptitle(index.title(), fontsize=20)
    plt.show()

categories = ['Phonology', 'Sign Languages', 'Other', 'Morphology', 'Nominal Categories', 'Nominal Syntax', 'Verbal Categories', 'Word Order', 'Simple Clauses', 'Complex Sentences', 'Lexicon']

# Statistics on the feature types.

print('THERE ARE ' + str(len(categories)) + ' FEATURE TYPES')
print('THERE ARE ' + str(len(w)) + ' WALS FEATURES')
print('----------------------------------------------------------------------------------------------------')

for i in range(len(categories)):
    print('There are ' + str(types[categories[i]]['n_features']) + ' features and ' +
          str(types[categories[i]]['n_values']) + ' values in ' + str(categories[i]))
    
total_features, total_values = [], []

for i in types:
    total_features.append(types[i]['n_features'])
    total_values.append(types[i]['n_values'])
    
# Build plot of feature stats.
    
sns.set(context="talk")
rs = np.random.RandomState(8)

f, (ax1, ax2) = plt.subplots(2, 1, figsize=(12, 8), sharex=True)
x = categories
y1 = total_features
sns.barplot(x=x, y=y1, palette="Set1", ax=ax1)
ax1.axhline(0, color="k", clip_on=False)
ax1.set_ylabel("Features", fontsize=20)
y2 = total_values
sns.barplot(x=x, y=y2, palette="Set1", ax=ax2)
ax2.axhline(0, color="k", clip_on=False)
ax2.set_ylabel("Possible values", fontsize=20)
sns.despine(bottom=True)
plt.xticks(np.arange(len(categories)) , categories, rotation=90, fontsize=15)
plt.tight_layout(h_pad=2)
plt.show()

# Build plots for classes, per feature type.

values=[]
idx=0

for x in feature_types:
    for y in feature_types[x]:
        values.append(w[y]['n_values'])
        
    count = Counter(values)
    types[categories[idx]]['features_stats'] = sorted(count.items())
    values, els, occ = [], [], []
    
    for i,k in types[categories[idx]]['features_stats']:
        els.append(i)
        occ.append(k)
        
    plt.figure()
    n=len(els)
    series = pd.Series(occ, index=els, name=categories[idx])
    series.plot.pie(figsize=(10, 10), colors=list_colors(n+1, color_list), autopct='%1.1f%%', fontsize=10)
    plt.show()
        
    idx+=1 

# Build plot for the percentages of feature presence.

pourcentages = []

for x in langs:
    pourcentages.append(langs[x]['%'])
    
f, ax1 = plt.subplots(1, 1, figsize=(12, 8), sharex=True)
sns.set(); np.random.seed(0)
sns.distplot(pourcentages, kde=True, bins=50)
sns.set_context("paper", rc={"axes.labelsize":36})
sns.set_style("darkgrid", {"axes.facecolor": ".9"})
plt.xlabel("% of features", fontsize=20)
plt.ylabel("Occurrences", fontsize=20)
plt.xticks(fontsize=15, rotation=0)
plt.yticks(fontsize=15, rotation=0)
plt.show()

# Find how often features are present in the dataset

lang_counts = defaultdict(lambda: 0)

for x in langs:
    for y in langs[x]:
        if langs[x][y] != 0:
            lang_counts[y]+=1
            
for el in elements:
    del lang_counts[el]

els, occ = [], []
idx=0

for x in feature_types:
    for y in feature_types[x]:
        els.append(y)
        occ.append(lang_counts[y])
    
    occ = [x*100 / len(langs) for x in occ]
    
    for k in range(len(occ)):
        print(str(els[k]) + ' ' + str(occ[k]))
        
    idx+=1
    els, occ = [], []

# Build plot of how often features are present in the languages, per language family. Top 100 languages.

num_languages=100
    
els, occ = [], []

for f in families:
    for e in families[f]:
        els.append(e)
        occ.append(float(families[f][e]))
        
max_els = np.argsort(occ)[-num_languages:]
max_langs, fam, perc = [],[],[]

for i in max_els:
    max_langs.append(els[i]) #top 100 languages
    fam.append(langs[els[i]]['family']) #top 100 families
    perc.append(occ[i])
    
top = pd.DataFrame(list(zip(max_langs, perc, fam)),
              columns=['language','perc', 'family'])
top = top.sort_values(by=['family', 'perc'])
num_langs = len(set(fam))
colors=list_colors(num_langs+1, color_list)
col = defaultdict(lambda: defaultdict(int))

idx=0
for i in set(fam):
    col[i] = idx
    idx+=1
    
idx=0
top['color'], top['name'] = "", ""
for i in range(len(top)):
    top['color'][i] = colors[col[top['family'][idx]]]
    top['name'][i] = langs[top['language'][i]]['Name']
    idx+=1
    
x_pos = [i for i, _ in enumerate(top['name'])]

plt.figure(figsize=(20,50))
plt.barh(x_pos, top['perc'], color=top['color'], label = list(top['name']))
plt.ylabel("Language", fontsize=20)
plt.xlabel("% of features present", fontsize=20)
plt.title("Top 100 languages with features present.", fontsize=30)
plt.yticks(x_pos, top['name'], fontsize=15)
plt.gca().invert_yaxis()
plt.xticks(fontsize=15, rotation=0)

leg = []
d=defaultdict(list)
for i in range(len(set(fam))):
    x1 = list(unique_everseen(top['color']))
    x2 = list(unique_everseen(top['family']))
    val = mpatches.Patch(color=x1[i], label=x2[i])
    leg.append(val)
    
plt.legend(handles=leg,bbox_to_anchor=(1.05, 1), fontsize=15)
plt.show()

ordered_by_hand = False

# Build plot of ordinal and categorical features, per type.

if ordered_by_hand:
    idx = 0
    perc, labs, order, unorder = [], [], [], []
      
    for i in feature_types:
        ordered, IDs = [], []
        for j in feature_types[i]:
            ordered.append(w[j]['Ordered'])
    
        c = Counter(ordered)
        tot=c[0]+c[1]
        order.append(c[1]*100/tot)
        unorder.append(c[0]*100/tot)
        labs.append(categories[idx])
        idx+=1
    
    fig = plt.figure(figsize=(20,5)) 
    fig_dims = (3, 2)

    pl1 = plt.bar(range(len(labs)), order, color='#b5ffb9', edgecolor='white', width=0.85)
    pl2 = plt.bar(range(len(labs)), unorder, bottom=order, color='#f9bc86', edgecolor='white', width=0.85)
    plt.xticks(range(len(labs)), labs, fontsize=15, rotation=45)
    plt.yticks(fontsize=15)
    plt.ylabel('%', fontsize=20)
    plt.legend((pl1[0], pl2[0]), ('Ordinal', 'Categorical'), fontsize=15,bbox_to_anchor=(1, 1))
    plt.show()

# Build plot of the top 100 languages with highest percentage in a certain feature type.

names, cols = [], []

for i in langs: 
    idx=0
    for j in feature_types: 
        count=0
        for k in feature_types[j]: 
            if langs[i][k] != 0:
                count+=1
        langs[i][categories[idx]]=count/len(j)
        idx+=1    
    names.append(i)

cols.append('language')
for k in categories:
    cols.append(k)
    
my_df  = pd.DataFrame(columns = cols)
my_df['language'] = names

for n in categories:
    vals=[]
    for i in langs:
        vals.append(langs[i][n])
    my_df[n]=vals
    
sort_by = 'Word Order'
num_languages=100
new_df = my_df.sort_values(sort_by)[-num_languages:]

x_pos = [i for i, _ in enumerate(new_df['language'])]

colors = list_colors(len(categories), color_list)

names, dt = [], []
for i in new_df['language']:
    names.append(langs[i]['Name'])

idx=0
for i in categories:
    
    trace = go.Bar(
        y=list(names),
        x=new_df[i],
        name=i,
        orientation = 'h',
        marker = dict(
            color = colors[idx],
            line = dict(
                color = colors[idx],
                width = 1)
        )
    )
    
    dt.append(trace)
    idx+=1
    
layout = go.Layout(barmode='stack', width=800, height=1200, xaxis=dict(title='Count of %'), 
                   yaxis=dict(tickmode='linear'),
                  margin=go.Margin(l=200,r=50,b=100,t=100,pad=4), plot_bgcolor='#c7c7c7')

fig = go.Figure(data=dt, layout=layout)
py.iplot(fig)

