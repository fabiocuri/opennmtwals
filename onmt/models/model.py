""" Onmt NMT Model base class definition """
import torch.nn as nn
import torch
from collections import defaultdict

def get_local_features(EmbeddingFeatures, MLPFeatureTypes, MLPGlobal, FeatureValues, FeatureTypes, SimulationLanguages, model_opt):

    features_row, features_concat_per_type, featuretypes_after_MLP = defaultdict(lambda: defaultdict(int)), defaultdict(lambda: defaultdict(int)), defaultdict(lambda: defaultdict(int))
    featuretypes_concat, mean_all_features = defaultdict(lambda: defaultdict(int)), defaultdict(lambda: defaultdict(int))

    for Language in SimulationLanguages:
        a=[]
        for FeatureType in FeatureTypes:
            l=[]
            for Feature in FeatureTypes[FeatureType]:

                # 1st step: Obtain rows of features in its original embeddings, given the feature value.
                features_row[Language][Feature] = EmbeddingFeatures[Feature](FeatureValues[Language][Feature]) 
                features_row[Language][Feature] = features_row[Language][Feature].view(1,len(features_row[Language][Feature]))
                l.append(features_row[Language][Feature])

            # 2nd step: Concatenate features of the same type.
            features_concat_per_type[Language][FeatureType] = torch.cat(l, dim=1)

            # 3rd step: Run a MLP, for each feature type.
            featuretypes_after_MLP[Language][FeatureType] = MLPFeatureTypes[FeatureType](features_concat_per_type[Language][FeatureType]) # size: 1 x rnn_size
            a.append(featuretypes_after_MLP[Language][FeatureType])

        # 4th step: Concatenate all 11 vectors of size 1 x rnn_size, per language.
        featuretypes_concat[Language] = torch.cat(a, dim=0) # size: 11 x rnn_size

        # 5th step: Take mean.
        mean_all_features[Language] = featuretypes_concat[Language].mean(0) 
        mean_all_features[Language] = mean_all_features[Language].view(1,model_opt.rnn_size) # size: 1 x rnn_size

    # 6th step: select WALS for both languages or target only.
    if model_opt.wals_model == 'EncoderTarget' or model_opt.wals_model == 'DecoderTarget':
        wals_features = mean_all_features[SimulationLanguages[1]] # size: 1 x rnn_size

    if model_opt.wals_model == 'EncoderBoth' or model_opt.wals_model == 'DecoderBoth':
        wals_features = torch.cat((mean_all_features[SimulationLanguages[0]], mean_all_features[SimulationLanguages[1]]) , 1) # size: 1 x 2.rnn_size

    if model_opt.wals_model == 'AttentionTarget':
        wals_features = featuretypes_concat[SimulationLanguages[1]] # size: 11 x rnn_size

    if model_opt.wals_model == 'AttentionBoth':
        wals_features = torch.cat((featuretypes_concat[SimulationLanguages[0]], featuretypes_concat[SimulationLanguages[1]]) , 1) # size: 11 x 2.rnn_size

    # 7th step: Run a last MLP and ReLu.
    wals_features = MLPGlobal(wals_features)   # size: 1 x [1 OR 11] x rnn_size

    if model_opt.wals_model == 'EncoderTarget' or model_opt.wals_model == 'DecoderTarget' or model_opt.wals_model == 'EncoderBoth' or model_opt.wals_model == 'DecoderBoth':
        # 8th step: Find number of encoder's layers and get the right size.
        wals_features = wals_features.repeat(model_opt.enc_layers,1,1)    # size: enc_layers x 1 x rnn_size

    if model_opt.wals_model == 'AttentionTarget' or model_opt.wals_model == 'AttentionBoth':
        # 8th step: Transpose
        wals_features = wals_features.transpose(0,1) # size: 11 x 1 x rnn_size

    return wals_features

def combine_enc_hidden_wals_proj(enc_hidden, wals_features):

    dec_init_state = []
    if isinstance(enc_hidden, tuple):
        for e in enc_hidden:
            # e.size() = enc_layers x batch_size x rnn_size
            dec_init_state.append(e + wals_features)
        dec_init_state = tuple(dec_init_state)
    else:
        dec_init_state = enc_hidden + wals_features
    return dec_init_state


class NMTModel(nn.Module):

    # Base-model
    """
    Core trainable object in OpenNMT. Implements a trainable interface
    for a simple, generic encoder + decoder model.

    Args:
      encoder (:obj:`EncoderBase`): an encoder object
      decoder (:obj:`RNNDecoderBase`): a decoder object
      multi<gpu (bool): setup for multigpu support
    """

    def __init__(self, encoder, decoder, multigpu=False):
        self.multigpu = multigpu
        super(NMTModel, self).__init__()
        self.encoder = encoder
        self.decoder = decoder

        print('Base model NMT')

    def forward(self, src, tgt, lengths, dec_state=None):
        """Forward propagate a `src` and `tgt` pair for training.
        Possible initialized with a beginning decoder state.

        Args:
            src (:obj:`Tensor`):
                a source sequence passed to encoder.
                typically for inputs this will be a padded :obj:`LongTensor`
                of size `[len x batch x features]`. however, may be an
                image or other generic input depending on encoder.
            tgt (:obj:`LongTensor`):
                 a target sequence of size `[tgt_len x batch]`.
            lengths(:obj:`LongTensor`): the src lengths, pre-padding `[batch]`.
            dec_state (:obj:`DecoderState`, optional): initial decoder state
        Returns:
            (:obj:`FloatTensor`, `dict`, :obj:`onmt.Models.DecoderState`):

                 * decoder output `[tgt_len x batch x hidden]`
                 * dictionary attention dists of `[tgt_len x batch x src_len]`
                 * final decoder state
        """
        tgt = tgt[:-1]  # exclude last target from inputs

        enc_final, memory_bank = self.encoder(src, lengths)
        enc_state = \
            self.decoder.init_decoder_state(src, memory_bank, enc_final)
        decoder_outputs, dec_state, attns = \
            self.decoder(tgt, memory_bank,
                         enc_state if dec_state is None
                         else dec_state,
                         memory_lengths=lengths)
        if self.multigpu:
            # Not yet supported on multi-gpu
            dec_state = None
            attns = None
        return decoder_outputs, attns, dec_state


class EncoderInitialization(nn.Module):

    """
    Model A: uses WALS features to initialize encoder's hidden state.

    Encoder: RNNEncoder
    Decoder: StdRNNDecoder
    """

    def __init__(self, encoder, decoder, EmbeddingFeatures, MLPFeatureTypes, MLPGlobal, FeatureValues, FeatureTypes, SimulationLanguages, model_opt):

        super(EncoderInitialization, self).__init__()
        self.encoder = encoder  
        self.decoder = decoder  
        self.EmbeddingFeatures = EmbeddingFeatures  
        self.MLPFeatureTypes = MLPFeatureTypes 
        self.MLPGlobal = MLPGlobal 
        self.FeatureValues = FeatureValues 
        self.FeatureTypes = FeatureTypes
        self.SimulationLanguages = SimulationLanguages
        self.model_opt = model_opt

    def forward(self, src, tgt, lengths, dec_state=None):

        wals_features = get_local_features(self.EmbeddingFeatures, self.MLPFeatureTypes, self.MLPGlobal, self.FeatureValues, self.FeatureTypes, self.SimulationLanguages, self.model_opt)

        print(wals_features)

        # Find mini-batch size and get the right size.
        dim0, dim1, dim2 = src.size()
        wals_features = wals_features.repeat(1,dim1,1) # size: enc_layers x batch_size x rnn_size

        tgt = tgt[:-1]  # exclude last target from inputs
        wals_features = [wals_features, wals_features]  # To make it work under rnn inside of RNNEncoder.
        enc_hidden, context = self.encoder(src, lengths, wals_features)
        enc_state = self.decoder.init_decoder_state(src, context, enc_hidden)
        out, dec_state, attns = self.decoder(tgt, context,
                                             enc_state if dec_state is None
                                             else dec_state,
                                             lengths)

        return out, attns, dec_state

class DecoderInitialization(nn.Module):

    """
    Model B: adds WALS features to the encoder's output to initialize decoder's hidden state.

    Encoder: RNNEncoder
    Decoder: StdRNNDecoder
    """

    def __init__(self, encoder, decoder, EmbeddingFeatures, MLPFeatureTypes, MLPGlobal, FeatureValues, FeatureTypes, SimulationLanguages, model_opt):

        super(DecoderInitialization, self).__init__()
        self.encoder = encoder  
        self.decoder = decoder  
        self.EmbeddingFeatures = EmbeddingFeatures  
        self.MLPFeatureTypes = MLPFeatureTypes 
        self.MLPGlobal = MLPGlobal 
        self.FeatureValues = FeatureValues 
        self.FeatureTypes = FeatureTypes
        self.SimulationLanguages = SimulationLanguages
        self.model_opt = model_opt

    def forward(self, src, tgt, lengths, dec_state=None):

        wals_features = get_local_features(self.EmbeddingFeatures, self.MLPFeatureTypes, self.MLPGlobal, self.FeatureValues, self.FeatureTypes, self.SimulationLanguages, self.model_opt)

        print(wals_features)

        # Find mini-batch size and get the right size.
        dim0, dim1, dim2 = src.size()
        wals_features = wals_features.repeat(1,dim1,1) # size: enc_layers x batch_size x rnn_size

        tgt = tgt[:-1]  # exclude last target from inputs
        enc_hidden, context = self.encoder(src, lengths)
        dec_init_state = combine_enc_hidden_wals_proj(enc_hidden, wals_features)
        enc_state = self.decoder.init_decoder_state(src, context, dec_init_state)
        out, dec_state, attns = self.decoder(tgt, context,
                                             enc_state if dec_state is None
                                             else dec_state,
                                             lengths)

        return out, attns, dec_state


class AttentionWals(nn.Module):

    """
    Model C: the WALS features are incorporated into the attention mechanism.

    Encoder: RNNEncoder
    Decoder: StdRNNDecoderDoublyAttentive
    """

    def __init__(self, encoder, decoder, EmbeddingFeatures, MLPFeatureTypes, MLPGlobal, FeatureValues, FeatureTypes, SimulationLanguages, model_opt):

        super(AttentionWals, self).__init__()
        self.encoder = encoder  
        self.decoder = decoder  
        self.EmbeddingFeatures = EmbeddingFeatures  
        self.MLPFeatureTypes = MLPFeatureTypes 
        self.MLPGlobal = MLPGlobal 
        self.FeatureValues = FeatureValues 
        self.FeatureTypes = FeatureTypes
        self.SimulationLanguages = SimulationLanguages
        self.model_opt = model_opt

    def forward(self, src, tgt, lengths, dec_state=None):

        wals_features = get_local_features(self.EmbeddingFeatures, self.MLPFeatureTypes, self.MLPGlobal, self.FeatureValues, self.FeatureTypes, self.SimulationLanguages, self.model_opt)

        print(wals_features)

        # Find mini-batch size and get the right size.
        dim0, dim1, dim2 = src.size()
        wals_features = wals_features.repeat(1,dim1,1) # size: 11 x batch_size x rnn_size
        
        tgt = tgt[:-1]  # exclude last target from inputs
        enc_hidden, context = self.encoder(src, lengths)
        enc_state = self.decoder.init_decoder_state(src, context, wals_features, enc_hidden)
        out, out_wals, dec_state, attns = self.decoder(tgt, context, wals_features,
                                                       enc_state if dec_state is None
                                                       else dec_state,
                                                       lengths)

        out_total = out + out_wals

        return out_total, attns, dec_state
